#include <iostream>

#define N 20

void printfodd(int k, bool odd = true)
{
    for (int i = 0; i < k; ++i)
    {
        if ((odd)? i % 2 : !(i % 2)) std::cout << i << "\n";
    }

}

int main()
{
    printfodd(N,false);
    return 0;
}